FROM ubi8/s2i-core:rhel8.7

WORKDIR /camejuanm-web-revamp

COPY package*.json ./ 

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
